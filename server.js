(function() {
    var app = require('express')();
    var server = require('http').Server(app);
    var io = require('socket.io')(server);
    var Ball = require('./Ball');

    server.listen(8080);

    app.get('/pong', function(req, res) {
        res.sendFile(__dirname + '/index.html');
    });

    app.get('/it.js', function(req, res) {
        res.sendFile(__dirname + '/it.js');
    });

    app.get('/pong.js', function(req, res) {
        res.sendFile(__dirname + '/pong.js');
    });

    var clients = [];
    var players = [];
    var ball = null;
    var time = 0;
    var loop = null;

    io.on('connection', function(socket) {
        newConnection(socket);
        socket.on('disconnect', disconnect);
        socket.on('mv_p', function(data) {
            socket.broadcast.emit('mv_p', { id: data.id, y: data.y });
        });
    });

    function newConnection(socket) {
        var pNum = 0;
        if (players.length > 0) {
            if (players[0].pNum == 0) pNum = 1;
            if (players[0].pNum == 1) pNum = 0;
        }

        socket.emit('conn', { id: socket.id, pNum: pNum });
        clients.push(socket);
        socket.on('new_p', newPlayer);
    }

    function disconnect(socket) {
        var socketIndex = clients.indexOf(socket);
        clients.splice(socketIndex, 1);
        players.splice(getPlayerIndex(socketIndex), 1);
    }

    function newPlayer(data) {
        if (ball == null)
            ball = new Ball();

        io.sockets.emit('new_b', { x: ball.x, y: ball.y });
        players.push(data);

        if (players.length > 1) {
            time = Date.now();
            loop = setInterval(gameLoop, 50);
        }
    }

    function getPlayerIndex(id) {
        players.map(function(player, i) {
            if (player.id === id) return i;
        });
    }

    function getPlayerByPnum(pNum) {
        players.map(function(player) {
            if (player.pNum === pNum) return player;
        });
    }


    function gameLoop() {
        ball.update((Date.now() - time) / 1000);

        io.sockets.emit('mv_b', { x: ball.x, y: ball.y });
        time = Date.now();
    }

})();