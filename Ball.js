function Ball() {
    this.r = .0;
    this.x = .5;
    this.y = .5;
    this.vx = .2;
    this.vy = .2;
    this.directionX = (Math.random() -.5) * 2;
    this.directionY = (Math.random() -.5) * 2;

    this.update = function(mod) {
        if (this.x <= 0 || this.x + this.r >= 1)
            return false;
        if (this.y <= 0 || this.y + this.r >= 1)
            this.directionY *= -1;

        this.y += (this.vy * this.directionY) * mod;
        return true;
    };

    this.reverse = function(distFromPaddleCenter) {
        this.x += (this.vx * this.directionX);
        console.log(distFromPaddleCenter);
    }
}

module.exports = Ball;