(function() {
    var socket,
        canvas,
        player,
        other,
        game,
        pnum;

    window.addEventListener('load', function () {
        canvas = document.getElementById('canvas');
        connect(canvas);
    });

    function connect(canvas) {
        var p1_pos = {
            x: 10,
            y: (canvas.height - Paddle.prototype.H) / 2
        };
        var p2_pos = {
            x: canvas.width - 10 - Paddle.prototype.W,
            y: (canvas.height - Paddle.prototype.H) / 2
        };

        socket = io.connect('http://do.aquilleph.com');
        socket.on('conn', function(data) {
            if (pnum == null)
                pnum = data.pNum;

            var pname = document.getElementById('p-name');
            pname.innerHTML = 'Player ' + (pnum + 1);

            if (pnum === 0) {
                player = new Paddle(p1_pos.x, p1_pos.y);
                other = new Paddle(p2_pos.x, p2_pos.y);
            } else {
                player = new Paddle(p2_pos.x, p2_pos.y);
                other = new Paddle(p1_pos.x, p1_pos.y);
            }

            socket.emit('new_p', {id: data.id, pNum: data.pNum, pos: player.pos});
            init();
        });
    }

    function init() {
        game = new Game(player, other, canvas);
        setEventHandlers();
    }

    function setEventHandlers(){
        socket.on('new_b', function(data) {
            if (game.ball === null) {
                game.ball = new Ball(data.x * canvas.width, data.y * canvas.height);
            }
        });

        socket.on('mv_b', function(data) {
            if (game.ball !== null) {
                game.ball.setPosition(data.x * canvas.width, data.y * canvas.height);
            }
        });

        socket.on('mv_p', function(data) {
            game.onPlayerMoved(data, other, game.size);
        });
    }

    var Game = function(player, other, canvas) {
        var self = this;
        this.player = player;
        this.other = other;
        this.ball = null;

        this.ctrl = new Keyboarder();
        this.ctx = canvas.getContext('2d');
        this.size = {
            x: canvas.width,
            y: canvas.height
        };

        this.tick = function () {
            self.update();
            self.draw();
            requestAnimationFrame(self.tick);
        };

        this.update = function() {
            if (this.ctrl.isDown(this.ctrl.keys.UP)) {
                this.player.up();
                socket.emit('mv_p', { id: socket.id, y: this.player.pos.y / this.size.y });
            }

            if (this.ctrl.isDown(this.ctrl.keys.DOWN)) {
                this.player.down();
                socket.emit('mv_p', { id: socket.id, y: this.player.pos.y / this.size.y });
            }
        };

        this.draw = function() {
            this.ctx.clearRect(0, 0, this.size.x, this.size.y);
            this.player.draw(this.ctx);
            this.other.draw(this.ctx);
            if (this.ball) {
                this.ball.draw(this.ctx);
            }
        };

        this.onPlayerMoved = function(data, other, size) {
            other.pos.y = data.y * size.y;
        };

        this.tick();
    };


    var Controller = function(body) {

    };



    var Keyboarder = function () {
        var keyState = {};
        window.addEventListener('keydown', function(e) {
            keyState[e.keyCode] = true;
        });

        window.addEventListener('keyup', function(e) {
            keyState[e.keyCode] = false;
        });

        this.isDown = function(keyCode) {
            return keyState[keyCode] === true;
        };

        this.keys = { UP: 38, DOWN: 40, SPACE: 32 };
    };


    /**
     * @class
     * @classdesc Abstract class representing a game body. Parent class of Ball and Paddle.
     *
     * @constructor
     * Creates a Body object, called by subclass constructor.
     * @param {object} size - Object with keys x, y containing the width and height of the body.
     * @param {object} pos  - Object with keys x, y containing the x & y coordinates of the top left of the body.
     */
    var Body = function (size, pos) {
        this.size = size;
        this.pos = pos;
    };

    Body.prototype = {
        constructor: Body,
        TYPES: {Local: 'local', Remote: 'remote' },
        type: Body.prototype.TYPES.Remote,
        size: {x: null, y: null},
        pos: {x: null, y: null},
        color: "#fff",
        draw: function (ctx) {
            ctx.fillStyle = this.color;
            ctx.beginPath();
            ctx.fillRect(this.pos.x, this.pos.y, this.size.x, this.size.y);
            ctx.closePath();
        },
        setPosition: function (x, y) {
            var maxX = canvas.width - this.size.x;
            var maxY = canvas.height - this.size.y;
            if (x >= maxX) x = maxX;
            if (y >= maxY) y = maxY;
            if (y <= 0) y = 0;
            if (x <= 0) x = 0;

            this.pos = {x: x, y: y};
        },
        bottom: function () {
            return this.pos.y + this.size.y;
        },
        right: function () {
            return this.pos.x + this.size.x;
        }
    };


    /**
     * @class
     * @classdesc Class representing the ball in a Pong game.
     */
    var Ball = function(x, y) {
        var size = {x: this.RAD, y: this.RAD};
        var pos = {x: x, y: y};
        // Call parent constructor
        Body.call(this, size, pos);
    };

    Ball.prototype = Object.create(Body.prototype);
    it.ext(Ball.prototype, {
        RAD: 8
    });


    /**
     * @class
     * @classdesc Class representing a paddle in a Pong game.
     *
     * @param {int} x - Initial x coordinate of the new paddle.
     * @param {int} y - Initial y coordinate of the new paddle.
     */
    var Paddle = function (x, y) {
        var size = {x: this.W, y: this.H};
        var pos = {x: x, y: y};

        // Call parent constructor
        Body.call(this, size, pos);
    };

    Paddle.ext = it.ext;

    Paddle.prototype = Object.create(Body.prototype);
    it.ext(Paddle.prototype, {
        W: 10,
        H: 70,
        speed: 5,
        up: function () {
            this.setPosition(this.pos.x, this.pos.y - this.speed);
        },
        down: function () {
            this.setPosition(this.pos.x, this.pos.y + this.speed);
        }
    });
})();
