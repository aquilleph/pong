/**
 * Created by acsherrock on 1/26/15.
 */

it = (function() {
    /* Establish the root object.
     *     `window` in the browser, or
     *     `exports` on the server. */
    var root = this;

    /* Save the previous state of the it variable. */
    var previousIT = root.it;

    /* Create a safe reference to the IT object for use below */
    var it = function(obj) {
        if (obj instanceof it) return obj;
        if (!(this instanceof it)) return new it(obj);
        this._wrap = obj;
    };

    /* Export it object for Node.js. Or add `it` as a global object if in a browser. */
    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined') {
            exports = module.exports = it;
        }

        exports.it = it;
    } else {
        root.it = it;
    }

    /* Current it utils version */
    it.VERSION = '0.0.0';

    it.isObject = function(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
    };

    it.ext = function(dest) {
        if (!it.isObject(dest))
            return dest;

        for (var i = 1; i < arguments.length; ++i) {
            var src = arguments[i];
            for (var prop in src) {
                if (hasOwnProperty.call(src, prop)) {
                    dest[prop] = src[prop];
                }
            }
        }
        return dest;
    };

    return it;
})();
